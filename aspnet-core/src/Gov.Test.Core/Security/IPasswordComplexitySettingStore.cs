﻿using System.Threading.Tasks;

namespace Gov.Test.Security
{
    public interface IPasswordComplexitySettingStore
    {
        Task<PasswordComplexitySetting> GetSettingsAsync();
    }
}
