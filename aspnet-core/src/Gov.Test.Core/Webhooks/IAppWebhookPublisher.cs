﻿using System.Threading.Tasks;
using Gov.Test.Authorization.Users;

namespace Gov.Test.WebHooks
{
    public interface IAppWebhookPublisher
    {
        Task PublishTestWebhook();
    }
}
