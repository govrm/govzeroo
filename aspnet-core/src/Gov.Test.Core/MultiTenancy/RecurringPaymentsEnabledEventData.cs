﻿using Abp.Events.Bus;

namespace Gov.Test.MultiTenancy
{
    public class RecurringPaymentsEnabledEventData : EventData
    {
        public int TenantId { get; set; }
    }
}