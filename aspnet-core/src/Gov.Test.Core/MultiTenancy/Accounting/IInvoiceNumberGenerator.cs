﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace Gov.Test.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}