﻿using System.Collections.Generic;

namespace Gov.Test.DashboardCustomization
{
    public class Dashboard
    {
        public string DashboardName { get; set; }

        public List<Page> Pages { get; set; }
    }
}
