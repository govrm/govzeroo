﻿using System.Threading.Tasks;

namespace Gov.Test.Net.Sms
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}