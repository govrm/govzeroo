﻿using System;
using Gov.Test.Core;
using Gov.Test.Core.Dependency;
using Gov.Test.Services.Permission;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Gov.Test.Extensions.MarkupExtensions
{
    [ContentProperty("Text")]
    public class HasPermissionExtension : IMarkupExtension
    {
        public string Text { get; set; }
        
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (ApplicationBootstrapper.AbpBootstrapper == null || Text == null)
            {
                return false;
            }

            var permissionService = DependencyResolver.Resolve<IPermissionService>();
            return permissionService.HasPermission(Text);
        }
    }
}