﻿using Abp.AutoMapper;
using Gov.Test.Organizations.Dto;

namespace Gov.Test.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}