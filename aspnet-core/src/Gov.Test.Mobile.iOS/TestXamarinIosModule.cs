﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Gov.Test
{
    [DependsOn(typeof(TestXamarinSharedModule))]
    public class TestXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TestXamarinIosModule).GetAssembly());
        }
    }
}