﻿using Abp.AspNetCore.Mvc.Views;

namespace Gov.Test.Web.Views
{
    public abstract class TestRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected TestRazorPage()
        {
            LocalizationSourceName = TestConsts.LocalizationSourceName;
        }
    }
}
