﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using Gov.Test.Dto;

namespace Gov.Test.Gdpr
{
    public interface IUserCollectedDataProvider
    {
        Task<List<FileDto>> GetFiles(UserIdentifier user);
    }
}
