﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Gov.Test.MultiTenancy.HostDashboard.Dto;

namespace Gov.Test.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}