﻿using System.Collections.Generic;
using Gov.Test.Authorization.Users.Dto;
using Gov.Test.Dto;

namespace Gov.Test.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}