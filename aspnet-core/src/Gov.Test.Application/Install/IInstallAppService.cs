﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Gov.Test.Install.Dto;

namespace Gov.Test.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}