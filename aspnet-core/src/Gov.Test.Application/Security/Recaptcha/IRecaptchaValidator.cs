﻿using System.Threading.Tasks;

namespace Gov.Test.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}