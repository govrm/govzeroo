﻿using System.Threading.Tasks;
using Gov.Test.Sessions.Dto;

namespace Gov.Test.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
