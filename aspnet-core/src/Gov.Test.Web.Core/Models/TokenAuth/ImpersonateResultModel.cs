﻿namespace Gov.Test.Web.Models.TokenAuth
{
    public class ImpersonateResultModel
    {
        public string ImpersonationToken { get; set; }
    }
}