﻿using System.Collections.Generic;
using Gov.Test.DashboardCustomization.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.CustomizableDashboard
{
    public class AddWidgetViewModel
    {
        public List<WidgetOutput> Widgets { get; set; }

        public string DashboardName { get; set; }

        public string PageId { get; set; }
    }
}
