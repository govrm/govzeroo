﻿using System.Linq;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Gov.Test.Authorization.Users.Dto;
using Gov.Test.Security;
using Gov.Test.Web.Areas.AppAreaName.Models.Common;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Users
{
    [AutoMapFrom(typeof(GetUserForEditOutput))]
    public class CreateOrEditUserModalViewModel : GetUserForEditOutput, IOrganizationUnitsEditViewModel
    {
        public bool CanChangeUserName => User.UserName != AbpUserBase.AdminUserName;

        public int AssignedRoleCount
        {
            get { return Roles.Count(r => r.IsAssigned); }
        }

        public bool IsEditMode => User.Id.HasValue;

        public PasswordComplexitySetting PasswordComplexitySetting { get; set; }
    }
}