﻿using Abp.AutoMapper;
using Gov.Test.Authorization.Users;
using Gov.Test.Authorization.Users.Dto;
using Gov.Test.Web.Areas.AppAreaName.Models.Common;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Users
{
    [AutoMapFrom(typeof(GetUserPermissionsForEditOutput))]
    public class UserPermissionsEditViewModel : GetUserPermissionsForEditOutput, IPermissionsEditViewModel
    {
        public User User { get; set; }
    }
}