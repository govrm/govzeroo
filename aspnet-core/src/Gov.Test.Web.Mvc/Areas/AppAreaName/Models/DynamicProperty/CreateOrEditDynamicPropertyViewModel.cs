﻿using System.Collections.Generic;
using Gov.Test.DynamicEntityProperties.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.DynamicProperty
{
    public class CreateOrEditDynamicPropertyViewModel
    {
        public DynamicPropertyDto DynamicPropertyDto { get; set; }

        public List<string> AllowedInputTypes { get; set; }
    }
}
