﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Gov.Test.Editions.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Common
{
    public interface IFeatureEditViewModel
    {
        List<NameValueDto> FeatureValues { get; set; }

        List<FlatFeatureDto> Features { get; set; }
    }
}