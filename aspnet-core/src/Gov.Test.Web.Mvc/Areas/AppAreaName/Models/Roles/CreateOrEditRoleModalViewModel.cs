﻿using Abp.AutoMapper;
using Gov.Test.Authorization.Roles.Dto;
using Gov.Test.Web.Areas.AppAreaName.Models.Common;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class CreateOrEditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool IsEditMode => Role.Id.HasValue;
    }
}