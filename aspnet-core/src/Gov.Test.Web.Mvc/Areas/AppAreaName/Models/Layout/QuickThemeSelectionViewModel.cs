﻿namespace Gov.Test.Web.Areas.AppAreaName.Models.Layout
{
    public class QuickThemeSelectionViewModel
    {
        public string CssClass { get; set; }
    }
}
