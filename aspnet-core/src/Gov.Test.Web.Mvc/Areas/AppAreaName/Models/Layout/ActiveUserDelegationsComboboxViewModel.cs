﻿using System.Collections.Generic;
using Gov.Test.Authorization.Delegation;
using Gov.Test.Authorization.Users.Delegation.Dto;

namespace Gov.Test.Web.Areas.AppAreaName.Models.Layout
{
    public class ActiveUserDelegationsComboboxViewModel
    {
        public IUserDelegationConfiguration UserDelegationConfiguration { get; set; }
        
        public List<UserDelegationDto> UserDelegations { get; set; }
    }
}
