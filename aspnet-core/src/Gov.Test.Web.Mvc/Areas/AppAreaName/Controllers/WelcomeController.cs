﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using Gov.Test.Web.Controllers;

namespace Gov.Test.Web.Areas.AppAreaName.Controllers
{
    [Area("AppAreaName")]
    [AbpMvcAuthorize]
    public class WelcomeController : TestControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}