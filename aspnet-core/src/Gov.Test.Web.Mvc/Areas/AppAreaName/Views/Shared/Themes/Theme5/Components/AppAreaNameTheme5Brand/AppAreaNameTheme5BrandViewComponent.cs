﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Gov.Test.Web.Areas.AppAreaName.Models.Layout;
using Gov.Test.Web.Session;
using Gov.Test.Web.Views;

namespace Gov.Test.Web.Areas.AppAreaName.Views.Shared.Themes.Theme5.Components.AppAreaNameTheme5Brand
{
    public class AppAreaNameTheme5BrandViewComponent : TestViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppAreaNameTheme5BrandViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var headerModel = new HeaderViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync()
            };

            return View(headerModel);
        }
    }
}
