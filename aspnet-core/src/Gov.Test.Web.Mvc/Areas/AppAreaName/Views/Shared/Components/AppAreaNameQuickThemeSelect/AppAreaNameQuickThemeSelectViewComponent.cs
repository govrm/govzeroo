﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Gov.Test.Web.Areas.AppAreaName.Models.Layout;
using Gov.Test.Web.Views;

namespace Gov.Test.Web.Areas.AppAreaName.Views.Shared.Components.
    AppAreaNameQuickThemeSelect
{
    public class AppAreaNameQuickThemeSelectViewComponent : TestViewComponent
    {
        public Task<IViewComponentResult> InvokeAsync(string cssClass)
        {
            return Task.FromResult<IViewComponentResult>(View(new QuickThemeSelectionViewModel
            {
                CssClass = cssClass
            }));
        }
    }
}
