﻿using Gov.Test.MultiTenancy.Payments;

namespace Gov.Test.Web.Models.Payment
{
    public class CancelPaymentModel
    {
        public string PaymentId { get; set; }

        public SubscriptionPaymentGatewayType Gateway { get; set; }
    }
}