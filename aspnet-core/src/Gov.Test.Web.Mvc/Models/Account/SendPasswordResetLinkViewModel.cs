﻿using System.ComponentModel.DataAnnotations;

namespace Gov.Test.Web.Models.Account
{
    public class SendPasswordResetLinkViewModel
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}