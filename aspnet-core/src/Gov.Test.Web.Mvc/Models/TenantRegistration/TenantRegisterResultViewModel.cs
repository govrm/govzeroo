﻿using Abp.AutoMapper;
using Gov.Test.MultiTenancy.Dto;

namespace Gov.Test.Web.Models.TenantRegistration
{
    [AutoMapFrom(typeof(RegisterTenantOutput))]
    public class TenantRegisterResultViewModel : RegisterTenantOutput
    {
        public string TenantLoginAddress { get; set; }
    }
}