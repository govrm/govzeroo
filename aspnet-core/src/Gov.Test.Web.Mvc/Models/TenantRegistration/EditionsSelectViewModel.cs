﻿using Abp.AutoMapper;
using Gov.Test.MultiTenancy.Dto;

namespace Gov.Test.Web.Models.TenantRegistration
{
    [AutoMapFrom(typeof(EditionsSelectOutput))]
    public class EditionsSelectViewModel : EditionsSelectOutput
    {
    }
}
