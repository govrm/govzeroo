﻿using System.Threading.Tasks;
using Abp.Webhooks;

namespace Gov.Test.WebHooks
{
    public interface IWebhookEventAppService
    {
        Task<WebhookEvent> Get(string id);
    }
}
