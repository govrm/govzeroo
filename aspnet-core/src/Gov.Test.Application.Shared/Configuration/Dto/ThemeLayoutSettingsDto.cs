﻿namespace Gov.Test.Configuration.Dto
{
    public class ThemeLayoutSettingsDto
    {
        public string LayoutType { get; set; }
    }
}