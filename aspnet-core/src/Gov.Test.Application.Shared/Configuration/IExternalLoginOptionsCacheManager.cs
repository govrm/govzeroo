﻿namespace Gov.Test.Configuration
{
    public interface IExternalLoginOptionsCacheManager
    {
        void ClearCache();
    }
}
