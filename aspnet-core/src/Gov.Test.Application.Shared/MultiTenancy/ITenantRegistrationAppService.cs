﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Gov.Test.Editions.Dto;
using Gov.Test.MultiTenancy.Dto;

namespace Gov.Test.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}