﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace Gov.Test.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}
