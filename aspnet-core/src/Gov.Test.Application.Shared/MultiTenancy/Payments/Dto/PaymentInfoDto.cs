﻿using Gov.Test.Editions.Dto;

namespace Gov.Test.MultiTenancy.Payments.Dto
{
    public class PaymentInfoDto
    {
        public EditionSelectDto Edition { get; set; }

        public decimal AdditionalPrice { get; set; }

        public bool IsLessThanMinimumUpgradePaymentAmount()
        {
            return AdditionalPrice < TestConsts.MinimumUpgradePaymentAmount;
        }
    }
}
