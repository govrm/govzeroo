﻿namespace Gov.Test.Tenants.Dashboard.Dto
{
    public class GetProfitShareOutput
    {
        public int[] ProfitShares { get; set; }
    }
}