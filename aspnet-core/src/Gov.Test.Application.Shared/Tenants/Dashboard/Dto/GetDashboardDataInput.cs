﻿namespace Gov.Test.Tenants.Dashboard.Dto
{
    public class GetDashboardDataInput
    {
        public SalesSummaryDatePeriod SalesSummaryDatePeriod { get; set; }
    }
}