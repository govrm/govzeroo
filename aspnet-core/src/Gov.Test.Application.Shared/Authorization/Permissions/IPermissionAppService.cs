﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Gov.Test.Authorization.Permissions.Dto;

namespace Gov.Test.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
