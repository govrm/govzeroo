﻿using System.ComponentModel.DataAnnotations;

namespace Gov.Test.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}