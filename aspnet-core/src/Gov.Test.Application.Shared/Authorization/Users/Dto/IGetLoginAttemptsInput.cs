﻿using Abp.Application.Services.Dto;

namespace Gov.Test.Authorization.Users.Dto
{
    public interface IGetLoginAttemptsInput: ISortedResultRequest
    {
        string Filter { get; set; }
    }
}