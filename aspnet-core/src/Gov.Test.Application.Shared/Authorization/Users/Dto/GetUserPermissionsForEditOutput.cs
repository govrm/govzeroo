﻿using System.Collections.Generic;
using Gov.Test.Authorization.Permissions.Dto;

namespace Gov.Test.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}