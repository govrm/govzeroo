﻿using System.ComponentModel.DataAnnotations;

namespace Gov.Test.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
