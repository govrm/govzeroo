﻿namespace Gov.Test.DynamicEntityProperties.Dto
{
    public class GetAllEntitiesHasDynamicPropertyOutput
    {
        public string EntityFullName { get; set; }
    }
}