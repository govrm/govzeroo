﻿using Abp.Application.Services;
using Gov.Test.Dto;
using Gov.Test.Logging.Dto;

namespace Gov.Test.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
