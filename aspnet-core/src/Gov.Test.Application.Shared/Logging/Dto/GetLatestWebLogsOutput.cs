﻿using System.Collections.Generic;

namespace Gov.Test.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
