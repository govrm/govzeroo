﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Gov.Test
{
    [DependsOn(typeof(TestXamarinSharedModule))]
    public class TestXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TestXamarinAndroidModule).GetAssembly());
        }
    }
}