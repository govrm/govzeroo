﻿using Microsoft.AspNetCore.Mvc;
using Gov.Test.Web.Controllers;

namespace Gov.Test.Web.Public.Controllers
{
    public class AboutController : TestControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}