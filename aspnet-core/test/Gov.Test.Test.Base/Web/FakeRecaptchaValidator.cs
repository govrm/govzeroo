﻿using System.Threading.Tasks;
using Gov.Test.Security.Recaptcha;

namespace Gov.Test.Test.Base.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
