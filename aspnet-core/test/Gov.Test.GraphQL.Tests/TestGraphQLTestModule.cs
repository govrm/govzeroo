﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Gov.Test.Configure;
using Gov.Test.Startup;
using Gov.Test.Test.Base;

namespace Gov.Test.GraphQL.Tests
{
    [DependsOn(
        typeof(TestGraphQLModule),
        typeof(TestTestBaseModule))]
    public class TestGraphQLTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            IServiceCollection services = new ServiceCollection();
            
            services.AddAndConfigureGraphQL();

            WindsorRegistrationHelper.CreateServiceProvider(IocManager.IocContainer, services);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(TestGraphQLTestModule).GetAssembly());
        }
    }
}