﻿using Abp.Modules;
using Gov.Test.Test.Base;

namespace Gov.Test.Tests
{
    [DependsOn(typeof(TestTestBaseModule))]
    public class TestTestModule : AbpModule
    {
       
    }
}
